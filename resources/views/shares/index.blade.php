
@extends('layouts.app')

@section('content')

<div class="container table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Company Name</th>
            <th>Share Instrument Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total Investment</th>
            <th>Certificate Number</th>
            <th>Transaction Date</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($shares as $share)
            <tr>
                <td>{{$share->id}}</td>
                <td>{{$share->company_name}}</td>
                <td>{{$share->share_instrument_name}}</td>
                <td>{{$share->quantity}}</td>
                <td>${{$share->price}}</td>
                <td>${{$share->total_investment}}</td>
                <td>{{$share->certificate_number}}</td>
                <td>{{$share->transaction_date}}</td>
                <td>
                    <a style="cursor: pointer" href="/shares/{{$share->id}}/edit">
                        Edit
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a class="btn btn-primary" style='float:right;' href="/shares/create">Create Request</a>
    {{$shares->render()}}

</div>

@endsection
