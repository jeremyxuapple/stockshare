@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Purchase Reqset From</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('shares.update', [$share->id]) }}">
                        {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="company_name" class="col-md-4 control-label">Company Name <span style="color:red">*</span></label>

                                <div class="col-md-6">
                                    <input id="company_name" type="text" class="form-control" name="company_name" value="{{ $share->company_name }}" required autofocus>

                                    @if ($errors->has('company_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('share_instrument_name') ? ' has-error' : '' }}">
                                <label for="share_instrument_name" class="col-md-4 control-label">Share Instrument Name<span style="color:red">*</span></label>

                                <div class="col-md-6">
                                    <input id="share_instrument_name" type="text" class="form-control" name="share_instrument_name" value="{{ $share->share_instrument_name }}" required autofocus>
                                    <select class="custom-select" id="inputGroupSelect01" onchange="selectOnChange()">
                                        <option selected>Choose...</option>
                                        <option value="A,B">A,B</option>
                                        <option value="Preferred">Preferred</option>
                                        <option value="Preferred">Preferred A,B</option>
                                    </select>
                                    @if ($errors->has('share_instrument_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('share_instrument_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label for="quantity" class="col-md-4 control-label">Quantity<span style="color:red">*</span></label>

                                <div class="col-md-6">
                                    <input id="quantity" type="text" class="form-control" name="quantity" value="{{ $share->quantity }}" required autofocus>

                                    @if ($errors->has('quantity'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Price<span style="color:red">*</span></label>

                                <div class="col-md-6">
                                    <input id="price" type="text" class="form-control" name="price" value="{{ $share->price }}" required autofocus>

                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group{{ $errors->has('total_investment') ? ' has-error' : '' }}">--}}
                                {{--<label for="total_investment" class="col-md-4 control-label">Total Investment<span style="color:red">*</span></label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="total_investment" type="text" class="form-control" name="total_investment" value="{{ $share->total_investment }}" required autofocus>--}}

                                    {{--@if ($errors->has('total_investment'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('total_investment') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group{{ $errors->has('certificate_number') ? ' has-error' : '' }}">
                                <label for="certificate_number" class="col-md-4 control-label">Certificate Number<span style="color:red">*</span></label>

                                <div class="col-md-6">
                                    <input id="certificate_number" type="text" class="form-control" name="certificate_number" value="{{ $share->certificate_number }}" required autofocus>

                                    @if ($errors->has('certificate_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('certificate_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>

                        </form>
                        <a class="btn btn-default" href="/">
                            Back
                        </a>
                        <a href="#" onclick="deleteConfirm()" class="btn btn-danger">
                            Delete
                        </a>

                        <form id="delete-form" action="{{ route('shares.destroy',[$share->id]) }}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
