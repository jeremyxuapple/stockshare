<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
      return redirect('/shares');
    } else {
      return redirect('/login');
    }

});

Route::get('/home', function(){
    return redirect('/shares');
});

Auth::routes();
Route::resource('shares', 'SharesController');


Route::post('/manualAuth', 'ManualLoginController@authenticate')->name('manualAuth');
Route::get('/showEmailUpdate', 'ManualLoginController@showEmailUpdate')->name('showEmailUpdate');
Route::post('/updateDefaultEmail', 'ManualLoginController@updateDefaultEmail')->name('updateDefaultEmail');
