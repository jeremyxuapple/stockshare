<?php

namespace Tests\Feature;

use App\Http\Controllers\Auth\LoginController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\UserEmailAddresses;
class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

        factory(User::class, 1)->create();

        $id = DB::getPdo()->lastInsertId();
        $user = User::where('id', $id)->get();
        $email = $user->email;
        $password = $user->password;

        $defaultEmail = factory(UserEmailAddresses::class)
            ->create(['is_default' => 1, 'user_id'=> $id, 'email' => $email]);

        factory(UserEmailAddresses::class)
            ->create(['is_default' => 0, 'user_id' => $id]);

        factory(UserEmailAddresses::class)
            ->create(['is_default' => 0, 'user_id' =>$id]);

        $loginController = new LoginController();

        $request1 = new \stdClass();
        $input1 = new \stdClass();
        $input1->email = $defaultEmail;
        $input1->password = $password;
        $request1->input = $input1;


        $this->assertTrue($loginController->attemptLogin($request1));
    }
}
