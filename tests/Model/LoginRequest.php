<?php

namespace Test\Model;

use Illuminate\Database\Eloquent\Model;

class LoginRequest extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'email',
        'password'
    ];


}
