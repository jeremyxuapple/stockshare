<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Share extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'shares';

    protected $fillable = [
        'company_name',
        'share_instrument_name',
        'quantity',
        'price',
        'total_investment',
        'certificate_number',
        'transaction_date',
        'user_id'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        $this->belogsTo('App\User');
    }
}
