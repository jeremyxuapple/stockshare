<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmailAddresses extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'email',
        'is_default',
    ];
}
