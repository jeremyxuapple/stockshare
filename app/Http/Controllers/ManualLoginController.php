<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Share;
use App\UserEmailAddresses;

class ManualLoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $email = $request->input('email');

        $default_email = DB::table('user_email_addresses')->where('email', '=', $email)->get()->first();

        if ($default_email->is_default) {

          if (Auth::attempt(['email' => $email, 'password' => $request->input('password')])) {
                $user = DB::table('users')->where('email', '=', $email)->get()->first();
                Auth::loginUsingId($user->id);
              return redirect()->intended('/');
          } else {
            // Credentials failed
            return redirect()->intended('/login')->with('error', 'Login Failed. Please check your credentials');
          }

        } else {
          // Not using default email to login
          return redirect()->intended('/login')->with('error', 'Login Failed. Please use your default email to login');

        }

    }

    /**
     * Show the email update form
     *
     * @return form view
     */
    public function showEmailUpdate()
    {
        return view('auth.defaultEmail');
    }

    /**
     * Update the login default email
     *
     * @return void
     */
    public function updateDefaultEmail(Request $request)
    {
//        $request->validate([
//            'email' => 'required|string|email|max:255|unique:user_email_addresses',
//        ]);

        $id = Auth::user()->id;

        $email = DB::table('user_email_addresses')->where([
            ['user_id','=',  $id],
            ['email', '=', $request->input('email')]
        ])->first();

        if ($email == null) {
        // The email has not been inserted before
            DB::transaction(function () use (&$request, &$id) {
                $date_utc = new \DateTime(null, new \DateTimeZone("America/New_York"));
                // Update email the Users table
                DB::table('users')->where('id', '=', $id)->update([
                    'email' => $request->input('email'),
                    'updated_at' => $date_utc->format('Y-m-d H:i:s')
                ]);
                // change the original default email to 0
                DB::table('user_email_addresses')->where([
                    ['user_id','=',  $id],
                    ['is_default', '=', 1]
                ])->update([
                    'is_default' => 0,
                    'updated_at' => $date_utc->format('Y-m-d H:i:s')
                ]);

                DB::table('user_email_addresses')->insert([
                    'email' => $request->input('email'),
                    'user_id' => $id,
                    'is_default' => 1,
                    'created_at' => $date_utc->format('Y-m-d H:i:s'),
                    'updated_at' => $date_utc->format('Y-m-d H:i:s'),
                ]);

            });

        } else {
            // The email has already been inserted

            DB::transaction(function () use (&$request, &$id) {
                $date_utc = new \DateTime(null, new \DateTimeZone("America/New_York"));
                DB::table('users')->where('id', '=', $id)->update([
                    'email' => $request->input('email'),
                    'updated_at' => $date_utc->format('Y-m-d H:i:s')
                ]);

                // change the original default email to 0
                DB::table('user_email_addresses')->where([
                    ['user_id','=',  $id],
                    ['is_default', '=', 1]
                ])->update([
                    'is_default' => 0,
                    'updated_at' => $date_utc->format('Y-m-d H:i:s')
                ]);

                // set the new default email to 1
                DB::table('user_email_addresses')->where([
                    ['user_id','=',  $id],
                    ['email', '=', $request->input('email')]
                ])->update([
                    'is_default' => 1,
                    'updated_at' => $date_utc->format('Y-m-d H:i:s')
                ]);

            });

        }

        // Redirect to shares.index
        $shares = Share::where('user_id', $id)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return redirect()->route('shares.index', ['shares' => $shares])
            ->with('success', 'Default Login Email updated succesfully!');

    }
}
