<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Share;
use App\User;

class SharesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $user_id = Auth::id();
            $shares = Share::where('user_id', $user_id)
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return view('shares.index', ['shares' => $shares]);
        } else {
            return view('auth.login');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return view('shares.create');
        } else {
            return view('auth.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            //Validate the data

            $request->validate([
                'company_name' => 'required|string|max:255',
                'share_instrument_name' => 'required|string|max:255',
                'quantity' => 'required|numeric|integer',
                'price' => 'required|numeric|',
                'certificate_number' => 'required|string|max:255',
            ]);

            DB::transaction(function () use (&$request) {
                $date_utc = new \DateTime(null, new \DateTimeZone("America/New_York"));

                DB::table('shares')->insert([
                    'company_name' => $request->input('company_name'),
                    'share_instrument_name' => $request->input('share_instrument_name'),
                    'quantity' => $request->input('quantity'),
                    'price' => $request->input('price'),
                    'total_investment' => $request->input('price') * $request->input('quantity'),
                    'certificate_number' => $request->input('certificate_number'),
                    'transaction_date' => $date_utc->format('Y-m-d'),
                    'created_at' => $date_utc->format('Y-m-d H:i:s'),
                    'updated_at' => $date_utc->format('Y-m-d H:i:s'),
                    'user_id' => Auth::id()
                ]);
            });

            $user_id = Auth::id();
            $shares = Share::where('user_id', $user_id)
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return redirect()->route('shares.index', [
                'shares' => $shares,
            ])->with('success', 'Purchase Request has been received!');

        } else {
            return view('auth.login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if(Auth::check()){

            $share = Share::find($id);
            return view('shares.edit', ['share' => $share]);

        } else {
            return view('auth.login');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::check()){
            $request->validate([
                'company_name' => 'required|string|max:255',
                'share_instrument_name' => 'required|string|max:255',
                'quantity' => 'required|numeric|integer',
                'price' => 'required|numeric|',
                'certificate_number' => 'required|string|max:255',
            ]);

            DB::transaction(function () use (&$request, &$id) {

                $date_utc = new \DateTime(null, new \DateTimeZone("America/New_York"));
                $share = Share::find($id);

                DB::table('shares')->where('id', '=', $id)->update([
                    'company_name' => $request->input('company_name'),
                    'share_instrument_name' => $request->input('share_instrument_name'),
                    'quantity' => $request->input('quantity'),
                    'price' => $request->input('price'),
                    'total_investment' => $request->input('price') * $request->input('quantity'),
                    'certificate_number' => $request->input('certificate_number'),
                    'updated_at' => $date_utc->format('Y-m-d H:i:s'),
                    'created_at' => $share['created_at'],
                ]);

            });

            $user_id = Auth::id();
            $shares = Share::where('user_id', $user_id)
                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return redirect()->route('shares.index', ['shares' => $shares])
                ->with('success', 'Purchase Request has been update successfully!');
        } else {
            return view('auth.login');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::check()){
            $findShare = Share::find($id);

            if ($findShare->delete()) {
                //redirect
                return redirect()->route('shares.index')
                    ->with('success', 'Purchase request deleted successfully');
            }
            return back()->withInput()->with('error', 'Purchase Request could not be deleted');

        } else {
            return view('auth.login');
        }

    }
}
