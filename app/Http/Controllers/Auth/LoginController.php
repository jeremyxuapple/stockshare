<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\UserEmailAddresses;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $email = $request->input('email');

        $default_email = DB::table('user_email_addresses')->where('email', '=', $email)->get()->first();

        if ($default_email && !$default_email->is_default) {
            return false;
        }

        $password_validation = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );

        return ($password_validation);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        $email = $request->input('email');
        $default_email = DB::table('user_email_addresses')->where('email', '=', $email)->get()->first();

        // Load user from database
        $userEmail = UserEmailAddresses::where($this->username(), $request->{$this->username()})->first();
        $user = User::find($userEmail->user_id);

        // Check if user was successfully loaded, that the password matches
        // and defalut is not 1. If so, override the default error message.
        if ($user
            && \Hash::check($request->input('password'), $user->password)
            && $default_email
            && !$default_email->is_default) {
            $errors = [$this->username() => trans('auth.notDefault')];
        }


        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);

    }
}
