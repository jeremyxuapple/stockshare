<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserEmailAddresses;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:user_email_addresses',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $user = null;

       DB::transaction(function () use (&$data, &$user) {
        $date_utc = new \DateTime(null, new \DateTimeZone("America/New_York"));
          $id = DB::table('users')->insert([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'created_at' => $date_utc->format('Y-m-d H:i:s'),
            'updated_at' => $date_utc->format('Y-m-d H:i:s'),
          ]);

          $user_id = DB::getPdo()->lastInsertId();

          DB::table('user_email_addresses')->insert([
            'user_id' => $user_id,
            'email' => $data['email'],
            'is_default' => 1,
            'created_at' => $date_utc->format('Y-m-d H:i:s'),
            'updated_at' => $date_utc->format('Y-m-d H:i:s')
          ]);

          $user = User::find($user_id);
      });

      return $user;

    }
}
