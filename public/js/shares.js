function selectOnChange() {
    var instrument = $('#inputGroupSelect01').find(":selected").text();
    $('#share_instrument_name').val(instrument);
}

function deleteConfirm() {
    var result = confirm('Are you sure you wish to delete this Purchase Request?');
    if( result ){
        event.preventDefault();
        $('#delete-form').submit();
    }
}